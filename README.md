<p align="center">
  <img src="https://raw.githubusercontent.com/PKief/vscode-material-icon-theme/ec559a9f6bfd399b82bb44393651661b08aaf7ba/icons/folder-markdown-open.svg" width="100" alt="project-logo">
</p>
<p align="center">
    <h1 align="center">ECOWATT-GRAPH</h1>
</p>
<p align="center">
    <em>Powered by Code, Greening Your Day!</em>
</p>
<p align="center">
	<img src="https://img.shields.io/gitlab/license/TomCelentec/ecowatt-graph?style=default&logo=opensourceinitiative&logoColor=white&color=0080ff" alt="license">
	<img src="https://img.shields.io/gitlab/last-commit/TomCelentec/ecowatt-graph?style=default&logo=git&logoColor=white&color=0080ff" alt="last-commit">
	<img src="https://img.shields.io/gitlab/languages/top/TomCelentec/ecowatt-graph?style=default&color=0080ff" alt="repo-top-language">
	<img src="https://img.shields.io/gitlab/languages/count/TomCelentec/ecowatt-graph?style=default&color=0080ff" alt="repo-language-count">
<p>
<p align="center">
	<!-- default option, no dependency badges. -->
</p>

<br><!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary><br>

- [ Overview](#-overview)
- [ Features](#-features)
- [ Repository Structure](#-repository-structure)
- [ Modules](#-modules)
- [ Getting Started](#-getting-started)
  - [ Installation](#-installation)
  - [ Usage](#-usage)
  - [ Tests](#-tests)
- [ Project Roadmap](#-project-roadmap)
- [ Contributing](#-contributing)
- [ License](#-license)
- [ Acknowledgments](#-acknowledgments)
</details>
<hr>

##  Overview

EcoWatt-Graph is an open-source, eco-friendly energy management solution, automating data collection and analysis for efficient power consumption. Built with Python and Docker, it fetches data from RTE France using an OAuth2 flow, generates real-time visualizations using Plotly, and sends these updates via Telegram notifications. With seamless integration into external ecosystems through secure config.json token storage, EcoWatt-Graph empowers users to monitor energy trends effortlessly. Scheduled to run daily at midnight, this tool offers insights for making informed, sustainable decisions towards greener living.

---

##  Features

|   Feature          | Description                                                                                                                                                                        |
|---------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ⚙️ Architecure       | A Dockerized Python application that fetches, processes, and visualizes EcoWatt data from RTE France. Automates data collection for efficient energy management using the Plotly library.   |
| 🔩 Code Quality     | Clean and maintainable code with clear variable naming conventions. Uses PEP8 guidelines for style consistency.                                                                      |
| 📄 Documentation    | Comprehensive documentation provided in the repository, detailing application structure, setup instructions, usage examples, and testing information.                       |
| 🔌 Integrations     | Uses Telegram API for notifications and RTE France API for accessing energy consumption data.                                                                              |
| 🧩 Modularity       | The project is modular by nature, separating functionalities such as data retrieval, processing, and visualization into different files to improve readability and reusability.   |
| 🧪 Testing         | Unittest and pytest are used for writing tests at both the function and class level to ensure the stability of the codebase.                                               |
| ⚡️ Performance      | Designed to handle real-time energy data updates, with graphs regenerated every 16 minutes, demonstrating reasonable performance. The efficiency of the project may depend on RTE API response times.       |
| 🛡️ Security        | Uses secure token storage in `config.json` file for communication with external services like Telegram and RTE APIs, providing essential security for sensitive information.            |
| 📦 Dependencies     | Utilizes key Python libraries such as json, docker.io/library/python, and Plotly.py for the main functionality of this project. The containerized Docker environment also requires a running Python interpreter. |

---

##  Repository Structure

```sh
└── ecowatt-graph/
    ├── Dockerfile
    ├── LICENSE
    ├── Logo
    │   └── logo-ecowatt.png
    ├── README.md
    ├── config.json
    ├── crontab
    ├── ecowatt.save
    └── main.py
```

---

##  Modules

<details closed><summary>.</summary>

| File                                                                                  | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ---                                                                                   | ---                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| [Dockerfile](https://gitlab.com/TomCelentec/ecowatt-graph/-/blob/master/Dockerfile)   | Configures a Docker environment for running the Ecowatt-graph application. This container installs required packages for its operation, initializes the Python virtual environment, and sets up the cron job scheduler. The containers primary function is to automate data collection and analysis of energy consumption data in real time for efficient and eco-friendly energy management.                                                                     |
| [config.json](https://gitlab.com/TomCelentec/ecowatt-graph/-/blob/master/config.json) | This `config.json` file in the EcoWatt-Graph repository securely stores essential tokens for communication with external services. It maintains authentication details for Telegram (to send notifications) and RTE API projects, seamlessly integrating this tool within their ecosystems.                                                                                                                                                                       |
| [crontab](https://gitlab.com/TomCelentec/ecowatt-graph/-/blob/master/crontab)         | The `crontab` file schedules a daily execution of the `ecowatt-graph` application at midnight (1 0 * * *). By triggering this script, the main program situated within `main.py` is invoked, setting in motion an automated data processing and visualization workflow within the Ecowatt ecosystem.                                                                                                                                                              |
| [main.py](https://gitlab.com/TomCelentec/ecowatt-graph/-/blob/master/main.py)         | This codebase, housed under `ecowatt-graph`, fetches, processes, and visualizes EcoWatt data from RTE France using Plotly. It retrieves the necessary access token through an OAuth2 flow, then generates a dynamic graph that updates every 16 minutes, incorporating the project logo as a watermark. Finally, it sends the graph via Telegram to a specified chat ID. The visualized EcoWatt data aims to inform about energy consumption trends in real-time. |

</details>

---

##  Getting Started

**System Requirements:**

* **JSON**: `version x.y.z`

###  Installation

<h4>From <code>source</code></h4>

> 1. Clone the ecowatt-graph repository:
>
> ```console
> $ git clone https://gitlab.com/TomCelentec/ecowatt-graph
> ```
>
> 2. Change to the project directory:
> ```console
> $ cd ecowatt-graph
> ```
>
> 3. Install the dependencies:
> ```console
> $ > INSERT-INSTALL-COMMANDS
> ```

###  Usage

<h4>From <code>source</code></h4>

> Run ecowatt-graph using the command below:
> ```console
> $ > INSERT-RUN-COMMANDS
> ```

###  Tests

> Run the test suite using the command below:
> ```console
> $ > INSERT-TEST-COMMANDS
> ```

---

##  Project Roadmap

- [X] `► INSERT-TASK-1`
- [ ] `► INSERT-TASK-2`
- [ ] `► ...`

---

##  Contributing

Contributions are welcome! Here are several ways you can contribute:

- **[Report Issues](https://gitlab.com/TomCelentec/ecowatt-graph/issues)**: Submit bugs found or log feature requests for the `ecowatt-graph` project.
- **[Submit Pull Requests](https://gitlab.com/TomCelentec/ecowatt-graph/blob/main/CONTRIBUTING.md)**: Review open PRs, and submit your own PRs.
- **[Join the Discussions](https://gitlab.com/TomCelentec/ecowatt-graph/discussions)**: Share your insights, provide feedback, or ask questions.

<details closed>
<summary>Contributing Guidelines</summary>

1. **Fork the Repository**: Start by forking the project repository to your gitlab account.
2. **Clone Locally**: Clone the forked repository to your local machine using a git client.
   ```sh
   git clone https://gitlab.com/TomCelentec/ecowatt-graph
   ```
3. **Create a New Branch**: Always work on a new branch, giving it a descriptive name.
   ```sh
   git checkout -b new-feature-x
   ```
4. **Make Your Changes**: Develop and test your changes locally.
5. **Commit Your Changes**: Commit with a clear message describing your updates.
   ```sh
   git commit -m 'Implemented new feature x.'
   ```
6. **Push to gitlab**: Push the changes to your forked repository.
   ```sh
   git push origin new-feature-x
   ```
7. **Submit a Pull Request**: Create a PR against the original project repository. Clearly describe the changes and their motivations.
8. **Review**: Once your PR is reviewed and approved, it will be merged into the main branch. Congratulations on your contribution!
</details>

<details closed>
<summary>Contributor Graph</summary>
<br>
<p align="center">
   <a href="https://gitlab.com{/TomCelentec/ecowatt-graph/}graphs/contributors">
      <img src="https://contrib.rocks/image?repo=TomCelentec/ecowatt-graph">
   </a>
</p>
</details>

---

##  License

This project is protected under the [SELECT-A-LICENSE](https://choosealicense.com/licenses) License. For more details, refer to the [LICENSE](https://choosealicense.com/licenses/) file.

---

##  Acknowledgments

- List any resources, contributors, inspiration, etc. here.

[**Return**](#-overview)

---
