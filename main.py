import datetime
import json
import pickle
import os
from math import cos, pi

import plotly.graph_objects as go
import plotly.io as pio
import requests
import telegram
from PIL import Image

pio.kaleido.scope.default_format = "png"
pio.kaleido.scope.default_width = "1920"
pio.kaleido.scope.default_height = "1080"


def get_config():
    with open("config.json", "r") as f:
        config_data_string = f.read()
        config_data = json.loads(config_data_string)
    return config_data


def rte_get_oauth(config):
    url = "https://digital.iservices.rte-france.com/token/oauth/"
    headers = {
        "Authorization": "Basic " + config["rte"]["project_token"]}
    resp = requests.post(url, headers=headers)
    if resp.status_code == 200:
        return resp.json()["access_token"]
    else:
        telegram_bot = telegram.Bot(token=config["telegram"]["telegram_token"])
        telegram_bot.sendMessage(config["telegram"]["chat_id"], "Error retrieving OAuth2 token")
        raise Exception("Error retrieving OAuth2 token")


def ecowatt_get(token, config):
    with open("ecowatt.save", "rb") as f:
        data_ecowatt = pickle.load(f)

    date = data_ecowatt['time']

    if datetime.datetime.utcnow() - date > datetime.timedelta(minutes=16):
        url = "https://digital.iservices.rte-france.com/open_api/ecowatt/v4/signals"

        headers = {"Authorization": "Bearer " + token}
        resp = requests.get(url, headers=headers)

        if resp.status_code == 200:
            time = datetime.datetime.utcnow()
            data_ecowatt = resp.json()['signals']
            with open("ecowatt.save", "wb") as f:
                pickle.dump({'time': time, 'data': data_ecowatt}, f)
        else:
            telegram_bot = telegram.Bot(token=config["telegram"]["telegram_token"])
            telegram_bot.sendMessage(config["telegram"]["chat_id"], "Error retrieving EcoWatt data")
            raise Exception("Error retrieving EcoWatt data")


def ecowatt_process():
    with open("ecowatt.save", "rb") as f:
        data_ecowatt = pickle.load(f)

    signals = data_ecowatt["data"]

    date = []
    value = []

    for i in range(len(signals)):
        date_temp = datetime.datetime.fromisoformat(signals[i]['jour'])
        for j in range(len(signals[i]['values'])):
            pas = signals[i]['values'][j]['pas']
            hvalue = signals[i]['values'][j]['hvalue']
            delta = datetime.timedelta(hours=pas)

            date.append(date_temp + delta)
            value.append(hvalue)

    return date, value


def generate_graph():
    date, value = ecowatt_process()

    date_new = []
    value_new = []

    for i in range(len(date)):
        for j in range(360):
            if i < len(date) - 1:
                date_new.append(date[i] + datetime.timedelta(seconds=j * 10))
                value_new.append(value[i] + (value[i + 1] - value[i]) * 0.5 * (1 - cos(pi * (j / 360))))
            else:
                date_new.append(date[i] + datetime.timedelta(seconds=j * 10))
                value_new.append(value[i])

    img = Image.open('Logo/logo-ecowatt.png')

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=date_new, y=value_new, mode='markers', marker=dict(cmin=1, cmax=3, color=value_new,
                                                                                  colorscale=[[0, "rgb(2,240,198)"],
                                                                                              [0.5, "rgb(242,121,15)"],
                                                                                              [1, "rgb(230,57,70)"]])))
    fig.add_layout_image(
        dict(
            source=img,
            layer="above",
            xref="paper", yref="paper",
            x=-0.02, y=1.02,
            sizex=0.17, sizey=0.17,
            xanchor="left", yanchor="bottom"
        )
    )
    fig.update_yaxes(range=[0.9, 3.1], tickmode='linear', tick0=1, dtick=1, gridcolor="white")
    fig.update_xaxes(gridcolor="dimgrey")
    fig.update_layout(title="Ecowatt - " + date[0].date().strftime("%A %d %B %Y"), template="plotly_dark",
                      font=dict(size=24), title_x=0.5)
    # fig.show()
    fig.write_image("graph.png")


def send_telegram_message(config):
    with open("ecowatt.save", "rb") as f:
        data_ecowatt = pickle.load(f)

    signals = data_ecowatt["data"]


    token = config["telegram"]["telegram_token"]
    chat_id = config["telegram"]["chat_id"]

    telegram_bot = telegram.Bot(token=token)
    with open("graph.png", "rb") as f:
        telegram_bot.send_photo(chat_id, f, caption=signals[0]["message"])

    os.remove("graph.png")


def main():
    config = get_config()
    oauth2_token = rte_get_oauth(config)
    ecowatt_get(oauth2_token, config)
    generate_graph()
    send_telegram_message(config)


if __name__ == "__main__":
    main()
