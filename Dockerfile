FROM docker.io/library/python:3
MAINTAINER Clément COTE <tom.celentec@protonmail.com>

ENV TZ=Europe/Paris

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get -y upgrade && apt-get install -y cron

WORKDIR /home/

RUN python3 -m venv venv && venv/bin/pip3 install plotly kaleido requests python-telegram-bot Pillow

ADD . /home/

RUN cp crontab /etc/cron.d/ && chmod 0644 /etc/cron.d/crontab && crontab /etc/cron.d/crontab

CMD ["cron", "-f"]
